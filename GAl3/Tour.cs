﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;

namespace GAl3
{
    [DebuggerDisplay("Y={YValue} P={PValue} M={MValue}")]
    class Tour
    {
        readonly Generator _generator;

        readonly IReadOnlyList<int> _origin;

        public Tour(Generator generator, Func<List<City>, double> fitnessFunc, string originSource)
        {
            _generator = generator ?? throw new ArgumentNullException("generator");
            FitnessFunc = fitnessFunc ?? throw new ArgumentNullException("fitnessFunc");

            var source = File.ReadAllLines(originSource).Select(_=> int.Parse(_));

            _origin = new List<int>(source);
        }

        public Tour(Generator generator, Func<List<City>, double> fitnessFunc, 
            string tspSource, string originSource) 
            : this(generator, fitnessFunc, originSource)
        {
            var source = File.ReadAllLines(tspSource);

            foreach (var entry in source)
            {
                var parts = entry.Split(" ").ToList();
                parts.ForEach(_ => _.Trim());
                var city = new City(new Point(int.Parse(parts[1]), int.Parse(parts[2])), int.Parse(parts[0]));

                Genes.Add(city);
            }

            Genes = Shuffle(Genes);

            //CodedGenes = Map(Genes);

            //YValue = Math.Round(FitnessFunc.Invoke(Genes),4);
            YValue = FitnessFunc.Invoke(Genes);
        }

        public Tour(Generator generator, City[] genes, Func<List<City>, double> fitnessFunc, string originSource)
            : this(generator, fitnessFunc, originSource)
        {
            //CodedGenes = genes.ToList();

            //Genes = Unmap(CodedGenes);

            Genes = genes.ToList();

            //YValue = Math.Round(FitnessFunc.Invoke(Genes),16);
            YValue = FitnessFunc.Invoke(Genes);
        }

        public Guid Id { get; private set; } = Guid.NewGuid();

        public List<City> Genes { get; private set; } = new List<City>();

        //public List<City> CodedGenes { get; private set; } = new List<City>();

        public double YValue { get; private set; }

        public double PValue { get; private set; }

        public double MValue { get; private set; }

        Func<List<City>, double>  FitnessFunc;

        public void SetPValue(double populationMinYValue, double totalYValues)
        {
            PValue = (YValue - populationMinYValue) / totalYValues;
        }

        //public void SetMValue(int nValue) => MValue = PValue * nValue;

        public void SetMValue(int nValue)
        {
            //const double up = 1.4;
            //const double down = 0.6;

            MValue = PValue * nValue;

            //if (MValue < 1.0)
            //    MValue = MValue * down;
            //else
            //    MValue = MValue * up;
        }

        public double SuccessPercent()
        {
            var success = 0;
            for (int i = 0; i < _origin.Count; i++)
            {
                if (Genes[i].Number == _origin[i])
                    success++;
            }

            var percent = _origin.Count / 100.0;
            return success * percent;
        }

        List<City> Shuffle(List<City> genes)
        {
            int n = genes.Count - 1;
            while (n > 1)
            {
                // при перемешивании не трогаем первый город
                int k = _generator.Next(1, n);
                var temp = genes[n];
                genes[n] = genes[k];
                genes[k] = temp;
                --n;
            }

            return genes;
        }

        List<City> Map(List<City> genes)
        {            
            //тур T1=(1-2-4-3-8-5-9-6-7).
            //вектор n1=(2 4 8 3 9 7 1 5 6)            
            var vector = new City[genes.Count];

            for (int i = 0; i < genes.Count; i++)
            {
                if (i == 0)
                {
                    var lastNumber = genes[genes.Count - 1].Number - 1;
                    vector[lastNumber] = genes[0];
                }
                else
                {
                    var pos = genes[i - 1].Number - 1;
                    vector[pos] = genes[i];
                }                
            }

            return vector.ToList();
        }        

        List<City> Unmap(List<City> genes)
        {
            var vector = new City[genes.Count];

            vector[0] = genes.Single(_ => _.Number == 1);

            while (vector.Count(_=>_ != null) != genes.Count)
            {
                for (int i = 0; i < genes.Count; i++)
                {
                    if (vector.FirstOrDefault(_=>_?.Number == genes[i].Number) != null)
                        continue;

                    var prefixValue = genes.SingleOrDefault(_ => _.Number == i + 1);
                    var prefixValueIndex = vector.ToList().FindIndex(_ => _?.Number == prefixValue.Number);

                    if (prefixValueIndex != -1)
                        vector[++prefixValueIndex] = genes[i];
                }                
            }
            return vector.ToList();
        }            
    }
}
