﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GAl3
{
    class Func
    {
        
        //public static readonly Func<List<City>, double> GetTourDistanceWithEuclideanCoordinates = (cities) =>
        //{
        //    double Distance(double x1, double y1, double x2, double y2)
        //        => Math.Sqrt(((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));

        //    double total = 0;

        //    for (int i = 1; i < cities.Count; i++)
        //    {
        //        total += Distance(cities[i].Coordinates.X, cities[i].Coordinates.Y, 
        //            cities[i - 1].Coordinates.X, cities[i - 1].Coordinates.Y);
        //    }

        //    //return 10000 / total;
        //    return 1 / total;
        //};

        public static readonly Func<List<City>, double> GetTourDistanceWithEuclideanCoordinates = (cities) =>
        {

            //добавить дистанцию от последнего города до первого
            double Distance(double x1, double y1, double x2, double y2)
                => Math.Sqrt(((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));

            double total = 0;
            //от первого до последнего
            for (int i = 1; i < cities.Count; i++)
            {
                total += Distance(cities[i].Coordinates.X, cities[i].Coordinates.Y,
                    cities[i - 1].Coordinates.X, cities[i - 1].Coordinates.Y);
            }

            //от последнего до первого
            total += Distance(cities.Last().Coordinates.X, cities.Last().Coordinates.Y,
                cities.First().Coordinates.X, cities.First().Coordinates.Y);

            //var min = 

            //return 10000 / total  - 3;
            //return 10000 / total - 3.15;
            //return 1 / total * 2 + 2;
            //return total * 2;
            return 1000 / total;
            //return  Math.Pow(1 / total, 3);
        };
    }
}
