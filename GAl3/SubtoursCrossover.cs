﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GAl3
{
    /*
     * Обмен подтуров
Этот оператор строит потомки путём выбора (случайной длины) подтура из первого родителя,
затем выбора подтура (опять случайной длины) из второго родителя и их обмена.
Как и ранее в случае конфликта оператор случайно выбирает другое ребро (город) из не
вошедших в построенный тур.
     * */
    class SubtoursCrossover : ICrossover
    {
        readonly Generator _generator;

        public SubtoursCrossover(Generator generator) => _generator = generator;

        public List<City> Do(List<City> parent1, List<City> parent2)
        {

            var offspring = DoOffspring(parent1, parent2);

            return offspring;          
        }

        List<City> DoOffspring(List<City> father, List<City> mother)
        {
            var offspring = new City[father.Count];

            var subtourLength = _generator.Next(1, (int)Math.Round(father.Count * 0.3));

            var subtourIndexStartPosition = _generator.Next(2, father.Count - subtourLength - 1);

            for (int i = subtourIndexStartPosition; i < subtourIndexStartPosition + subtourLength; i++)
                offspring[i] = father[i];

            var s = offspring.Where(_ => _ != null).Select(_ => _).ToList();
            //var exceptSubtourNumbers = mother.Select(_=>_.Number).Except(s.Select(_ => _.Number)).ToList();

            var motherCopy = mother.ToList();

            foreach (var number in s)
                motherCopy.RemoveAll(_ => _.Number == number.Number);

            var shift = 0;

            for (int i = 0; i < mother.Count; i++)
            {
                if (offspring[i] == null)
                    offspring[i] = motherCopy[i - shift];
                else
                    shift++;
            }            
                       
            return offspring.ToList();
        }

        //    List<City> CreateChild(List<City> father, List<City> mother)
        //{
        //    var child = new List<City>(mother);

        //    var attemts = 0;

        //    do
        //    {
        //        var fullCitiesSet = father.OrderBy(_ => _.Number).ToList();
               
        //        //готовим подтур для второго родителя
        //        var subtourLength = _generator.Next(1, (int)Math.Round(father.Count * 0.5));

        //        var subtourIndexStartPosition = _generator.Next(2, father.Count - subtourLength - 1);

        //        var subTour1 = new City[subtourLength];

        //        father.CopyTo(subtourIndexStartPosition, subTour1, 0, subtourLength);

        //        for (int i = 0; i < subtourLength; i++)
        //            child[i + subtourIndexStartPosition] = subTour1[i];

        //        child = ControlShortCircuit(child, fullCitiesSet);
        //        attemts++;
        //    } while (attemts < 5 && CheckInvalidTour(child));

        //    return father;
        //}

        //List<City> ControlShortCircuit(List<City> tour, List<City> fullCitiesSet)
        //{
        //    var conflicted = tour.GroupBy(_ => _.Number)
        //        .Where(_ => _.Count() > 1)
        //        .Select(_ => _.Key).ToList();

        //    var unused = fullCitiesSet.Select(_ => _.Number)
        //        .Except(tour.Select(_ => _.Number)).ToList();           

        //    for (int i = 0; i < conflicted.Count; i++)
        //        tour[tour.FindIndex(_ => _.Number == conflicted[i])] = fullCitiesSet
        //            .Single(_ => _.Number == unused[i]);

        //    return tour;
        //}
        
        //public bool CheckInvalidTour(List<City> tour)
        //{
        //    var checkSet = new List<int>();

        //    var current = 1;

        //    for (int i = 1; i <= tour.Count; i++)
        //    {
        //        if (checkSet.Contains(current))
        //            return true;

        //        checkSet.Add(current);
        //        current = tour[current - 1].Number;                               
        //    }

        //    return false;
        //}
       // }
    }
}
