﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Text;

namespace GAl3
{
    [DebuggerDisplay("Number={Number}")]
    class City
    {
        public City(Point coordinates, int number)
        {
            Coordinates = coordinates;
            Number = number;
        }

        public Point Coordinates { get; private set; }

        public int Number { get; private set; }
    }
}
