﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GAl3
{
    interface ICrossover
    {       
        List<City> Do(List<City> parent1, List<City> parent2);
    }
}
