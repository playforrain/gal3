﻿using System;
using System.Collections.Generic;

namespace GAl3
{
    class Configuration
    {
        public Configuration(int populationPower, float crossingoverPropability,
            float mutationPropability, Func<List<City>, double> fitnessFunc, 
            string tspSourceFileLocation, string originSourceFileLocation)
        {
            PopulationPower = populationPower;
            CrossingoverPropability = crossingoverPropability;
            MutationPropability = mutationPropability;
            FitnessFunc = fitnessFunc;
            TSPSourceFileLocation = tspSourceFileLocation;
            TSPSourceFileLocation = tspSourceFileLocation;
            OriginSourceFileLocation = originSourceFileLocation;
        }

        public int PopulationPower { get; private set; }

        public float CrossingoverPropability { get; private set; }

        public float MutationPropability { get; private set; }

        public Func<List<City>, double> FitnessFunc { get; private set; }

        public string TSPSourceFileLocation { get; private set; }

        public string OriginSourceFileLocation { get; private set; }
    }
}
