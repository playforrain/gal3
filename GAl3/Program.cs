﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

namespace GAl3
{
    /// <summary>        
    /// 5 вариант
    /// https://www.sfu.ca/~ssurjano/rosen.html
    /// 
    /// 7 вариант
    /// https://www.sfu.ca/~ssurjano/schwef.html
    /// 
    /// 8 вариант
    /// eil76
    /// </summary>
    class Program
    {
        static readonly Generator _generator = new Generator();

        static void Main(string[] args)
        {
            //var d = Func.GetTourDistanceWithEuclideanCoordinates(new List<City>()
            //{
            //    new City(new System.Drawing.Point(1,5),1),
            //    new City(new System.Drawing.Point(4,78),2),
            //    new City(new System.Drawing.Point(214,82),3)
            //});  

            //var ch = new Tour(_generator, Func.GetTourDistanceWithEuclideanCoordinates);
            //var cr = new SubtoursCrossover(_generator);
            //var vector = new[]
            //{
            //    new City(new System.Drawing.Point(1,5),2),
            //    new City(new System.Drawing.Point(1,5),4),
            //    new City(new System.Drawing.Point(1,5),8),
            //    new City(new System.Drawing.Point(1,5),3),
            //    new City(new System.Drawing.Point(1,5),9),
            //    new City(new System.Drawing.Point(1,5),7),
            //    new City(new System.Drawing.Point(1,5),1),
            //    new City(new System.Drawing.Point(1,5),5),
            //    new City(new System.Drawing.Point(1,5),6)
            //};

            var vector = new[]
            {
                new City(new System.Drawing.Point(1,5),4),
                new City(new System.Drawing.Point(1,5),1),
                new City(new System.Drawing.Point(1,5),5),
                new City(new System.Drawing.Point(1,5),8),
                new City(new System.Drawing.Point(1,5),6),
                new City(new System.Drawing.Point(1,5),3),
                new City(new System.Drawing.Point(1,5),9),
                new City(new System.Drawing.Point(1,5),7),
                new City(new System.Drawing.Point(1,5),2)
            };

            //вектор n1=(2 4 8 3 9 7 1 5 6) 
            //тур T1=(1-2-4-3-8-5-9-6-7).    
            //var t = cr.CheckInvalidTour(vector.ToList());

            var configuration = new Configuration(
                500,
                0.5f,
                0.1f,
                Func.GetTourDistanceWithEuclideanCoordinates,
                "eil76.txt",
                "eil76or.txt"
                );           

            var population = new Population(_generator, configuration, null);

            var genes = population
                .Reproduction()
                .Select()
                .Crossingover()
                .Mutation()
                .Reduction();

            Console.WriteLine();
            Console.WriteLine($"====GENERATION 1=====");

            foreach (var item in genes)
                Console.Write($"{item.YValue} ");

            Console.WriteLine();

            var counter = 1;

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine($"====GENERATION {++counter}===FF={population.YAverageValue}==");

                population = new Population(_generator, configuration, genes);

                genes = population
                    .Reproduction()
                    .Select()
                    .Crossingover()
                    .Mutation()
                    .Reduction();

                foreach (var item in genes)
                    Console.Write($"({Math.Round(item.YValue,4)}, {item.SuccessPercent()}%)");

                Console.WriteLine();
                Console.WriteLine();

                var key = Console.ReadKey(true);

                if (key.KeyChar == 'q')
                    break;

            }

            var best = population.GetBestChromosomeGenesValues();

            Console.WriteLine("Значения генов лучшей хромосомы:");

            foreach (var item in best)
                Console.WriteLine(item);

            Console.WriteLine();

            Console.WriteLine("Нажмите и завершите...");

            Console.ReadKey();
        }
    }
}
