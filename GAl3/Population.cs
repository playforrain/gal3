﻿using System.Linq;
using System.Collections.Generic;
using System;

namespace GAl3
{
    class Population
    {
        readonly Generator _generator;

        readonly Configuration _configuration;

        readonly ICrossover _crossover;

        public double YAverageValue { get; private set; }

        public double YMaxValue { get; private set; }

        List<Tour> population;

        readonly List<Tour> selectedParents = new List<Tour>();

        readonly List<Tuple<Tour, Tour>> marriedParents = new List<Tuple<Tour, Tour>>();
        
        public Population(Generator generator, Configuration configuration, Tour[] genes)            
        {
            _generator = generator;
            _configuration = configuration;
            _crossover = new SubtoursCrossover(_generator);

            if (genes == null)
            {
                population = new List<Tour>();

                for (int i = 0; i < _configuration.PopulationPower; i++)
                {
                    var candidate = new Tour(_generator, _configuration.FitnessFunc, 
                        _configuration.TSPSourceFileLocation, _configuration.OriginSourceFileLocation);
                    
                    //?????????????
                    while (population.Any(_ => Enumerable.SequenceEqual(_.Genes, candidate.Genes)))
                        candidate = new Tour(_generator, _configuration.FitnessFunc,
                        _configuration.TSPSourceFileLocation, _configuration.OriginSourceFileLocation);


                    population.Add(candidate);                    
                }
            }
            else
                population = genes.ToList();

            double accum = 0;

            //var minYValue = population.Max(__ => __.YValue);

            var minYValue = 0;

            /*
                * для нахождения вероятности берем сумму абсолютных значений фитнес функции.
                * при неизвестном максимуме функции за верхнюю границу берем максимальное
                * значение фитнес функции в популяции
                */
            for (int i = 0; i < _configuration.PopulationPower; i++)
            {
                population[i].SetPValue(minYValue, population.Sum(_ => _.YValue - minYValue));                
                population[i].SetMValue(_configuration.PopulationPower);
                accum += population[i].YValue;               
            }

            YMaxValue = population.Max(_ => _.YValue);

            YAverageValue = accum / _configuration.PopulationPower;            
        }       
        
        public Population Reproduction()
        {

            for (int i = 0; i < _configuration.PopulationPower; i++)
            {                
                int count = (int)Math.Round(population[i].MValue);
                for (int j = 0; j < count; j++)
                        selectedParents.Add(population[i]);                                  
            }

            return this;
        }

        public Population Select()
        {
            //список отобранных для кроссинговера особей не пустой
            //и в нем содержиться более одной уникальной особи
            var checkGenes = selectedParents?.FirstOrDefault()?.Genes;
            if (checkGenes == null)
                throw new Exception("no future  parents");

            if (selectedParents.All(_ => Enumerable.SequenceEqual(_.Genes, checkGenes)))
                throw new Exception("only one parent in set");            

            do
            {
                var fatherIndex = _generator.Next(0, selectedParents.Count);
                var father = selectedParents.ToList()[fatherIndex];

                var motherIndex = _generator.Next(0, selectedParents.Count);
                var mother = selectedParents.ToList()[motherIndex];

                //while (fatherIndex == motherIndex || Enumerable.SequenceEqual(father.Genes, mother.Genes))
                while (fatherIndex == motherIndex || father.YValue == mother.YValue)
                {
                    motherIndex = _generator.Next(0, selectedParents.Count);
                    mother = selectedParents.ToList()[motherIndex];
                }
                    
                marriedParents.Add(Tuple.Create(father, mother));
            } while (marriedParents.Count < _configuration.PopulationPower / 2);                      

            return this;
        }


        public Population Crossingover()
        {                                   
            foreach (var entry in marriedParents)
            {
                /*
                 * если true, событие произошло и делаем кроссинговер,
                 * если false, кроссинговер не происходит, в след. поколение идут сами родители
                 */                                             
                if (_generator.GetProbability(_configuration.CrossingoverPropability))
                {
                    Tour newborn1, newborn2;

                    var newbornGenes1 = _crossover.Do(entry.Item1.Genes,
                            entry.Item2.Genes);
                    var newbornGenes2 = _crossover.Do(entry.Item1.Genes,
                            entry.Item2.Genes);

                    newborn1 = new Tour(_generator, newbornGenes1.ToArray(),
                        _configuration.FitnessFunc, _configuration.OriginSourceFileLocation);

                    newborn2 = new Tour(_generator, newbornGenes2.ToArray(),
                        _configuration.FitnessFunc, _configuration.OriginSourceFileLocation);                    

                    population.Add(newborn1);
                    population.Add(newborn2);

                }                                             
            }

            return this;
        }        

        public Population Mutation()
        {
            if (_generator.GetProbability(_configuration.MutationPropability))
            {
                var mutantIndex = _generator.Next(1, population.Count);
                var mutantGenes = population[mutantIndex].Genes;

                var mutantGeneIndex1 = _generator.Next(2, mutantGenes.Count);
                var mutantGeneValue = mutantGenes[mutantGeneIndex1];

                var mutantGeneIndex2 = _generator.Next(2, mutantGenes.Count);

                mutantGenes[mutantGeneIndex1] = mutantGenes[mutantGeneIndex2];

                mutantGenes[mutantGeneIndex2] = mutantGeneValue;

                //mutantGenes[mutantGeneIndex] = _generator.Next(1, population);
                var mutant = new Tour(_generator, mutantGenes.ToArray(), 
                    _configuration.FitnessFunc, _configuration.OriginSourceFileLocation);
                population[mutantIndex] = mutant;
            }      
            
            return this;
        }        
        
        public Tour[] Reduction()
        {
            var goodByeCount = population.Count - _configuration.PopulationPower;

            population = population.OrderByDescending(_ => _.YValue).ToList();

            population.RemoveRange(population.Count - goodByeCount, goodByeCount);

            return population.ToArray();
        }

        public IReadOnlyCollection<string> GetBestChromosomeGenesValues() =>
            population.First().Genes.Select(_ => _.ToString()).ToList();        
    }
}
